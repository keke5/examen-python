resultats = 0
articles = ''

while True: # tant que il n'y a pas d'instruction de fin
    x = input('Entrez un aliment et un prix (END pour quitter) : ') # Ce que le l'utilisateur entre
    if x == 'END': # Si ma condition est vérifiée
        print('Vous avez demandé: ' + articles)
        print('Le prix total est : ' + str(resultats)) # On affiche à la fin
        break # casse ma boucle

    string = x.split() # On sépare les arguments dans un tableau
    article = string[0] # Le nom de l'article est la 1er colonne du tableau
    prix = string[1] # Le prix est la 2eme colonne du tableau
    ok = False # A chaque boucle la verification est réinitialisée

    try: # donc on essaye de convertir en float le prix
        prix = float(prix)
    except ValueError: # Si il y a une erreur on s'arrete ici
        print("Le prix n'est pas un nombre.")
    else: # Sinon le test est verifié
        ok = True

    if ok == True: # Si le test est verifié
        resultats += prix # On rajoute au resultat total le prix de l'article actuel
        articles += article # On rajoute le nom de l'article actuel à la liste des articles
        articles += ' ' # On sépare les articles par un espace