while True: # tant que il n'y a pas d'instruction de fin
    x = input('Entrez un nombre (END pour quitter) : ') # Ce que le joueur entre
    if x == 'END': # Si ma condition est vérifiée
        print('Bye.') # Si c'est End je dis Bye
        break # casse ma boucle
    elif x.isdigit(): # Sinon si x est un nombre
        s = 0 # initialisation de la variable
        for i in x: # Pour chaque chiffre dans mon nombre
            s+= int(i) # Somme des chiffres du nombre
        print('Somme des chiffres : ' + str(s)) # On met en string pour le print
        if s%3==0: # Si le reste de la sommme diviser par 3 est égale à 0 alors
            print('OUI ! La somme des chiffres ' + str(s) + ' est divisible par 3')
            print('OUI ! Le nombre ' + str(x) + ' est divisible par 3')
        else:
            print('NON ! La somme des chiffres ' + str(s) + ' n’est pas divisible par 3')
            print('NON ! Le nombre ' + str(x) + ' n’est pas divisible par 3')
    else:
        print('Ce n’est pas un nombre entier.') # Si le joueur quelque chose qui n'est pas un entier